/**
 * Root file for the API
 */

// dependencies
const http = require("http");
const url = require("url");
const StringDecoder = require("string_decoder").StringDecoder;

const port = 3000;

const server = http.createServer(function (req, res) {
  let parsedUrl = url.parse(req.url, true);
  let path = parsedUrl.pathname;
  let trimmedPath = path.replace(/\+|\/+$/g, "");

  // Get query string object
  let queryStringObject = parsedUrl.query;

  // Get the HTTP method
  let method = req.method.toLocaleLowerCase();

  // Get the headers object
  let headers = req.headers;

  // Get the payload if there's any
  let decoder = new StringDecoder("utf-8");
  let buffer = "";
  req.on("data", (data) => {
    buffer += decoder.write(data);
  });

  req.on("end", () => {
    buffer += decoder.end();

    let chosenHandler = typeof(router[trimmedPath]) !== "undefined" ? router[trimmedPath] : handlers.notFound;

    let data = {
      trimmedPath,
      queryStringObject,
      method,
      headers,
      "payload": buffer
    };

    chosenHandler(data, function (statusCode, payload) {
      statusCode = typeof(statusCode) == 'number' ? statusCode : 200;
      payload = typeof(payload) == "object" ? payload : {};
      let payloadString = JSON.stringify(payload);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(statusCode);
      res.end(payloadString);
      console.log("Response: ", statusCode, payloadString);
    })
  });
});

// Define handlers
let handlers = {};

handlers.sample = function(data, callback) {
  callback(406, {"message": "hi there!"});
}
handlers.notFound = function (data, callback) {
  callback(404);
}

let router = {
  "/sample": handlers.sample,
}

// server listening at port 3000
server.listen(port, function() {
  console.log(`Listening at port ${port}`);
});